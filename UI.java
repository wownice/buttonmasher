import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class UI 
{
	private Stage stage;
	public static ArrayList<Macro> macroList; //static in order for MacroExecution to get during loops
	private MacroExecution execute;
	
	public static Robot robot; //the robot this program depends on
	
	static ConversionTable ct = new ConversionTable();
	
	public static ArrayList<Tab> tabArray;
	public static ListView<String> macroListView;
	public static CheckBox loopCheck;
	private ArrayList<Button> runButtonArray;
	private ComboBox<String> profileList;
	
	//variables for keyboard traversing comboboxes
	private String eventInput = "";
	private Timer eventTimer;
	
	public UI(Stage s) throws AWTException
	{
		stage = s;
		macroList = new ArrayList<Macro>();
		robot = new Robot();
		initializeUI();
	}
	
	public void initializeUI()
	{				
		//left side, keypress list - load macro details on item click
		ScrollPane keyPresses = new ScrollPane(); 
		macroListView = new ListView<String>();
		keyPresses.setContent(macroListView);
				
		//macro control
		VBox controlBox = new VBox(10);
		
		HBox controlButtonsBox = new HBox(10);
		controlButtonsBox.setAlignment(Pos.CENTER);
		Button playButton = new Button("Play");
		playButton.setTooltip(new Tooltip("Start from beginning or at last arrived macro."));	
		Button stopButton = new Button("Pause");
		stopButton.setTooltip(new Tooltip("Pause"));
		Button restartButton = new Button("Restart");
		restartButton.setTooltip(new Tooltip("Restart"));
		controlButtonsBox.getChildren().addAll(playButton, stopButton, restartButton);
			
		HBox startDelayBox = new HBox();
		Label startDelayLabel = new Label("Start Delay");
		startDelayLabel.setTooltip(new Tooltip("Time until macro begins. Default 1000 ms."));
		TextField startDelayText = new TextField("1000");
		startDelayBox.getChildren().addAll(startDelayLabel, startDelayText);
		HBox escapeHBox = new HBox();
		ComboBox<String> escapeButton = new ComboBox<String>();
		loopCheck = new CheckBox("Loop");
		Label escapeButtonLabel = new Label("Escape Button");
		escapeButtonLabel.setTooltip(new Tooltip("The button that will automatically end the macro when pressed."));
		escapeHBox.getChildren().addAll(escapeButtonLabel, escapeButton);
		controlBox.setAlignment(Pos.CENTER);
		controlBox.getChildren().addAll(controlButtonsBox, escapeHBox,loopCheck, startDelayBox);
		Tab runTab = new Tab();
		runTab.setText("Run");
		runTab.setContent(controlBox);
				
		//macro creation - adds to current scrollpane
		VBox creationSettings = new VBox(10);
		
		//toggle between mouse or keyboard
		HBox macroToggle = new HBox();
		macroToggle.setAlignment(Pos.CENTER);
		ToggleGroup macroToggleGroup = new ToggleGroup();
	    RadioButton keyboardMacroToggle = new RadioButton("Keyboard");
	    RadioButton mouseMacroToggle = new RadioButton("Mouse");
	    keyboardMacroToggle.setToggleGroup(macroToggleGroup);
	    mouseMacroToggle.setToggleGroup(macroToggleGroup);
	    keyboardMacroToggle.setSelected(true);
	    macroToggle.getChildren().addAll(keyboardMacroToggle, mouseMacroToggle);
		
		HBox keyType = new HBox();
		keyType.setAlignment(Pos.CENTER);
		ComboBox<String> inputKey = new ComboBox<String>();
		for (String key : ct.conversion.keySet())
		{
			escapeButton.getItems().add(key);
		    inputKey.getItems().add(key);
		}    		
		escapeButton.getSelectionModel().select("ESCAPE");
		inputKey.getSelectionModel().selectFirst();
		Label keyLabel = new Label("Key");
		keyLabel.setTooltip(new Tooltip("Key to be pressed."));
		keyType.getChildren().addAll(keyLabel, inputKey);
		
		HBox mouseCoordHBox = new HBox();
		mouseCoordHBox.setAlignment(Pos.CENTER);
		TextField mouseXTextField = new TextField("0");
		TextField mouseYTextField = new TextField("0");
		mouseXTextField.setMaxWidth(50);
		mouseYTextField.setMaxWidth(50);
		mouseCoordHBox.getChildren().addAll(new Label("X"), mouseXTextField, new Label("Y"), mouseYTextField);
		mouseCoordHBox.setDisable(true);
		
		//time setting
		HBox timeBox = new HBox();
		TextField timeText = new TextField("1000");
		Label timeLabel = new Label("Wait Time");
		timeText.setTooltip(new Tooltip("Time in milliseconds until the macro plays."));
		timeBox.getChildren().addAll(timeLabel, timeText);
		
		HBox listButtons = new HBox(10);
		listButtons.setAlignment(Pos.CENTER);
		Button addMacro = new Button("Add");		
		Button updateMacro = new Button("Update");
		Button removeMacro = new Button("Remove");
		removeMacro.setDisable(true); // should be enabled if clicking a current macro on listview
		listButtons.getChildren().addAll(addMacro, updateMacro, removeMacro);
		
		creationSettings.getChildren().addAll(macroToggle, keyType, mouseCoordHBox, timeBox, listButtons);
		Tab creationTab = new Tab();
		creationTab.setText("Edit");
		creationTab.setContent(creationSettings);
		
		//profiles - load listview items on selection + details of item 1
		VBox profilesBox = new VBox(10);
		profilesBox.setAlignment(Pos.CENTER);
		profileList = new ComboBox<String>();
		HBox profileButtons = new HBox(10);
		profileButtons.setAlignment(Pos.CENTER);
		Button reloadProfile = new Button("Reset");
		reloadProfile.setTooltip(new Tooltip("Resets current macro list to profile"));
		Button saveProfile = new Button("Add");
		Button deleteProfile = new Button("Delete");
		profileButtons.getChildren().addAll(reloadProfile, saveProfile, deleteProfile);
		HBox profileNameBox = new HBox();
		TextField profileNameTextField = new TextField();
		profileNameBox.getChildren().addAll(new Label("Profile Name"), profileNameTextField);
		profilesBox.getChildren().addAll(profileList, profileButtons, profileNameBox);			
		Tab profileTab = new Tab();
		profileTab.setText("Profiles");
		profileTab.setContent(profilesBox);
		loadProfilesToList();
		
		TabPane tabs = new TabPane();
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabs.getTabs().addAll(runTab, creationTab, profileTab);	
		tabArray = new ArrayList<Tab>();
		tabArray.add(creationTab);
		tabArray.add(profileTab);
		
		VBox managementBox = new VBox();
		managementBox.getChildren().addAll(controlBox, tabs);
		
		runButtonArray = new ArrayList<Button>();
		runButtonArray.add(playButton);
		runButtonArray.add(stopButton);
		runButtonArray.add(restartButton);
		disableRunButtons();
		
		//eventhandler methods
		keyboardMacroToggle.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				mouseCoordHBox.setDisable(true);
				inputKey.setDisable(false);
			}
		});
		
		mouseMacroToggle.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				mouseCoordHBox.setDisable(false);
				inputKey.setDisable(true);
			}
		});
		
		timeText.setOnKeyReleased(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	timeTextFieldFormat(timeText);
            }
		});
		
		startDelayText.setOnKeyReleased(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	timeTextFieldFormat(startDelayText);
            }
		});
		
		mouseXTextField.setOnKeyReleased(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	timeTextFieldFormat(mouseXTextField);
            }
		});
		
		mouseYTextField.setOnKeyReleased(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	timeTextFieldFormat(mouseYTextField);
            }
		});
				
		//keyboard input to traverse escapeButton
		escapeButton.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	keyInputComboBoxNavigation(event, escapeButton);
            }
		});
		
		//keyboard input to traverse inputKey
		inputKey.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	keyInputComboBoxNavigation(event, inputKey);
            }
		});
		
		//load profile macros on click
		profileList.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				loadProfileData(profileList.getSelectionModel().getSelectedItem());
			}
		});
		
		//current macro select
		macroListView.setOnMouseClicked(new EventHandler<MouseEvent>() 
		{
	        @Override
	        public void handle(MouseEvent event) 
	        {
	        	if(!macroListView.getSelectionModel().isEmpty())
	        	{
		        	int index = macroListView.getSelectionModel().getSelectedIndex();
		        	timeText.setText(macroList.get(index).getMillis() + "");
		        	if(macroList.get(index).getKeycode().equals("Mouse"))
		        	{
		        		mouseMacroToggle.setSelected(true);
		        		mouseXTextField.setText(macroList.get(index).getX() + "");
		        		mouseYTextField.setText(macroList.get(index).getY() + "");
		        		mouseCoordHBox.setDisable(false);
						inputKey.getSelectionModel().selectFirst();
						inputKey.setDisable(true);
		        	}
		        	else
		        	{
		        		keyboardMacroToggle.setSelected(true);
		        		String keycode = macroListView.getItems().get(
		        				macroListView.getSelectionModel().getSelectedIndex()).substring
		        				(0, macroListView.getItems().get(macroListView.getSelectionModel().getSelectedIndex())
		        						.indexOf(","));
		        		inputKey.getSelectionModel().select(keycode);
		        		mouseCoordHBox.setDisable(true);
						inputKey.setDisable(false);
		        	}
					removeMacro.setDisable(false);
	        	}
	        }
	    });
		
		//play from latest macro
		playButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				try 
				{
					if(execute == null)
						execute = new MacroExecution(macroList);
					else if(execute.finishedRun())
						execute.updateMacroList(macroList);
				} catch (AWTException e1) {}
											
				int time = intInputTest(startDelayText);
				if(time >= 0)
				{
					disableTabsDuringRun();
					execute.startTimer(time);
				}			
			}			
		});
		
		//pauses macro at latest macro
		stopButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				stopRun();
			}
		});
		
		//restarts from beginning of macro list
		restartButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				try 
				{
					if(execute != null)
						execute.stopTimer();
					execute = new MacroExecution(macroList);
				} catch (AWTException e1) {}
											
				int time = intInputTest(startDelayText);
				if(time >= 0)
				{
					disableTabsDuringRun();
					execute.startTimer(time);
				}				
			}
		});
		
		//adds macro to list
		addMacro.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				int time = intInputTest(timeText);
				if(time > 0)
				{
					time = Integer.parseInt(timeText.getText());
					Macro m;
					
					String listString;
					if(mouseMacroToggle.isSelected())
					{
						m = new MouseMacro(robot, "Mouse", time, Integer.parseInt(mouseXTextField.getText()),
								Integer.parseInt(mouseYTextField.getText()));
						listString = "Mouse, X: " +  mouseXTextField.getText() + ", Y: " + mouseYTextField.getText();
					}
					else
					{
						listString = inputKey.getSelectionModel().getSelectedItem().toString();
						m = new KeyboardMacro(robot, listString, time);
					}
					m.setMillis(time);
					macroList.add(m);
					macroListView.getItems().add(listString + ", " + m.getMillis() + "ms");
					macroListView.getSelectionModel().select(macroList.size() - 1);
					
					//if the macro exists and has been paused, restart
					if(execute != null)
					{
						if(execute.getMacros().size() != macroList.size() - 1)								
							execute.updateMacroList(macroList);
						else
							execute.getMacros().add(m);
					}
					
					if(macroList.size() >= 1)
					{
						removeMacro.setDisable(false);
					}
					
					enableRunButtons();					
				}
			}
		});
				
		updateMacro.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				int time = intInputTest(timeText);
				if(macroListView.getSelectionModel().getSelectedIndex() >= 0 && time > 0)
				{
					time = Integer.parseInt(timeText.getText());
					int index = macroListView.getSelectionModel().getSelectedIndex();
					
					String listString = "";
					if(mouseMacroToggle.isSelected())
					{
						macroList.set(index, new MouseMacro(robot, "Mouse", time, Integer.parseInt(mouseXTextField.getText()),
								Integer.parseInt(mouseYTextField.getText())));
						listString = "Mouse, X: " +  mouseXTextField.getText() + ", Y: " + mouseYTextField.getText();
					}
					else
					{
						listString = inputKey.getSelectionModel().getSelectedItem().toString();
						macroList.set(index, new KeyboardMacro(robot, listString, time));
					}
					
					macroList.get(index).setMillis(time); 
					macroListView.getItems().set(index, listString + ", " + time + "ms");
					macroListView.getSelectionModel().select(index);
					
					//if the macro exists and has been paused, restart
					if(execute != null)
					{
						if(execute.getMacros().size() - 1 != macroList.size())								
							execute.updateMacroList(macroList);
						else
							execute.getMacros().remove(index);
					}		
				}
			}
		});
		
		removeMacro.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				if(macroList.size() > 0)
				{					
					int index = macroListView.getSelectionModel().getSelectedIndex();					
					macroList.remove(index);
					macroListView.getItems().remove(index);			
					
					//selects the next macro upon removal
					if(index < macroList.size())
					{
						macroListView.getSelectionModel().select(index);
					}
					else
					{
						macroListView.getSelectionModel().select(index - 1);
					}
					
					//if the macro exists and has been paused, restart
					if(execute != null)
					{
						if(execute.getMacros().size() - 1 != macroList.size())								
							execute.updateMacroList(macroList);
						else
							execute.getMacros().remove(index);
					}					
				}					
				
				if(macroList.size() < 1)
				{
					removeMacro.setDisable(true);
				}
			}
		});
		
		//resets current macro list to what profile had
		reloadProfile.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				loadProfileData(profileList.getSelectionModel().getSelectedItem());
			}
		});
		
		saveProfile.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				try 
				{
					Profile.saveProfileFile(macroList, profileNameTextField.getText());
					loadProfilesToList();
				} catch (Exception e1) {e1.printStackTrace();}
			}
		});
		
		deleteProfile.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent e) 
			{
				Profile.deleteProfileFile(profileList.getSelectionModel().getSelectedItem());
			}
		});
		
		//keyboard input to traverse profile list
		profileList.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
    			eventInput += event.getText() + "";
    			int index = profileList.getSelectionModel().getSelectedIndex() + 1;
    			boolean found = false;
    			while(!found && index != profileList.getSelectionModel().getSelectedIndex())
    			{
    				if((ct.keyConversion[index] + "").toLowerCase().startsWith(eventInput.toLowerCase()))
					{
						found = true;
						profileList.getSelectionModel().select(index);
					}
    				index = (index + 1) % ct.conversion.size();
    			}
    			   			
    			if(eventTimer == null)
    			{
	            	eventTimer = new Timer();
	            	eventTimer.schedule(new TimerTask()
	    			{
	    				@Override
	    		        public void run()
	    				{
	    					eventInput = "";
	    					eventTimer.cancel();
	    					eventTimer = null;
	    		        }
	    			}, 500);
    			}
    		}          	          
		});
		
		//display
		HBox layoutBox = new HBox();
		layoutBox.getChildren().addAll(keyPresses, managementBox);
		Scene scene = new Scene(layoutBox);
		
		scene.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String input = event.getCode() + "";
            	String endInput;
            	if((input.length() >= 1 && input.length() <= 3 && input.charAt(0) == 'F') || input.equals("ESCAPE") || input.equals("CONTROL") ||
        				input.equals("ALT") || input.equals("SHIFT") || input.equals("ENTER") || input.equals("INSERT")
        				 || input.equals("PAGE_UP") || input.equals("PAGE_DOWN") || input.equals("HOME") || input.equals("END")
        				 || input.equals("DELETE"))
        		{
        			endInput = input;
        		}
        		else
        		{			
        			endInput = event.getText() + "";
        		}
            	
            	if(endInput.equals(escapeButton.getSelectionModel().getSelectedItem()))
    			{
            		stopRun();
    			}
            }
		});
		stage.getIcons().add(new Image("img/FEH_Death_Blow_3.png"));
		stage.setTitle("Button Masher");
		stage.setScene(scene);
		stage.show();
	}
	
	public void loadProfilesToList()
	{
		File f = new File("profiles/");
		if(f.exists())
		{
			String[] fileList = f.list();
			for(String s : fileList)
			{
				profileList.getItems().add(s.replaceFirst(".prof", ""));
			}
		}
	}

	public void keyInputComboBoxNavigation(KeyEvent event, ComboBox<String> box)
	{
		String input = event.getCode() + "";
		if((input.length() >= 1 && input.length() <= 3 && input.charAt(0) == 'F') || input.equals("ESCAPE") || input.equals("CONTROL") ||
				input.equals("ALT") || input.equals("SHIFT") || input.equals("ENTER") || input.equals("INSERT")
				 || input.equals("PAGE_UP") || input.equals("PAGE_DOWN") || input.equals("HOME") || input.equals("END")
				 || input.equals("DELETE"))
		{
			eventInput = input;
			if(eventTimer != null)
			{
				eventTimer.cancel();
				eventTimer = null;
			}
			box.getSelectionModel().select(eventInput);
		}
		else
		{			
			eventInput += event.getText() + "";
			int index = box.getSelectionModel().getSelectedIndex() + 1;
			boolean found = false;
			while(!found && index != box.getSelectionModel().getSelectedIndex())
			{
				if(index < ct.keyConversion.length)
				{
					if((ct.keyConversion[index] + "").toLowerCase().startsWith(eventInput.toLowerCase()))
					{
						found = true;
						box.getSelectionModel().select(index);
					}
					index = (index + 1) % ct.conversion.size();				
				}
				else
					index = 0;
			}
			
			if(eventTimer == null)
			{
	        	eventTimer = new Timer();
	        	eventTimer.schedule(new TimerTask()
				{
					@Override
			        public void run()
					{
						eventInput = "";
						eventTimer.cancel();
						eventTimer = null;
			        }
				}, 500);
			}
		}		   		
	} 
	
	public void stopRun()
	{
		if(execute != null)
		{
			execute.stopTimer();
			enableTabs();
		}
	}
	
	//tests if int is >= 0 and valid int - modifies label parameter if not valid
	public int intInputTest(TextField field)
	{
		try //try for int input only
		{
			int time = Integer.parseInt(field.getText().replaceAll("[^0-9]", ""));
			if(time > 0)
			{
				return time;
			}
		}
		catch(NumberFormatException ex){}			
		return 0;
	}
	
	public void loadProfileData(String fileName)
	{
		macroList = Profile.readProfileFile(profileList.getSelectionModel().getSelectedItem());
		execute = null;
		macroListView.getItems().clear();
		for(Macro m : macroList)
		{
			String addString = "";
			if(m.getKeycode().equals("Mouse"))
			{
				addString += "Mouse, X: " + m.getX() + ", Y: " + m.getY();
			}
			else
			{
				addString += m.getKeycode(); 
			}
			addString += ", " + m.getMillis() + "ms";
			macroListView.getItems().add(addString);
		}
	}

	public void timeTextFieldFormat(TextField f)
	{
		int prevPos = f.getCaretPosition();
    	f.setText(intInputTest(f) + "");
    	f.selectPositionCaret(prevPos);
    	f.deselect();
	}
	
	//tabs should be disabled when a macro is running
	public static void disableTabsDuringRun()
	{
		for(Tab t : tabArray)
			t.setDisable(true);
		loopCheck.setDisable(true);
	}
	
	public static void enableTabs()
	{
		for(Tab t : tabArray)
			t.setDisable(false);
		loopCheck.setDisable(false);
	}
	
	public void disableRunButtons()
	{
		for(Button b : runButtonArray)
			b.setDisable(true);
		loopCheck.setDisable(true);
	}
	
	public void enableRunButtons()
	{
		for(Button b : runButtonArray)
			b.setDisable(false);
		loopCheck.setDisable(false);
	}
}
