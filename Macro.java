import java.awt.Robot;

public abstract class Macro 
{
	private Robot robot;
	private int millis;
	private String keycode;
	
	public Macro(Robot rob, String key, int mil)
	{
		robot = rob;
		keycode = key;
		millis = mil;
	}

	public void setMillis(int m)
	{
		millis = m;
	}
	
	public int getMillis()
	{
		return millis;
	}
	
	public String getKeycode()
	{
		return keycode;
	}
	
	public Robot getRobot()
	{
		return robot;
	}
	
	public abstract void pressButton();
	
	//mouse macro methods
	public abstract int getX();
	
	public abstract int getY();
}