import java.awt.AWTException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MacroExecution 
{
	private ArrayList<Macro> macros;
	private Macro currentMacro;
	private Timer timer;
	
	public MacroExecution(ArrayList<Macro> q) throws AWTException
	{
		updateMacroList(q);
	}

	public void startTimer(int time) //starts the timer after a second
	{		
		UI.macroListView.getSelectionModel().select(0);
		Timer tempTimer = new Timer();
		if(macros.size() > 0)
		{
			tempTimer.schedule(new TimerTask()
			{
				@Override
		        public void run()
				{
					playTimer();
					tempTimer.cancel();
					tempTimer.purge();
		        }
			}, time);
		}
	}
	
	public void playTimer()
	{
		stopTimer();
		if(macros.size() > 0)
		{
			timer = new Timer();
			currentMacro = macros.remove(0);
			//System.out.println("Pressing: " + currentMacro.getKeyCode());
			timer.schedule(new TimerTask()
			{
				@Override
		        public void run()
				{
					int size = UI.macroList.size() - macros.size();
					if(size < 0)
						size = 0;
					UI.macroListView.getSelectionModel().select(size);
					currentMacro.pressButton();
					cancel(); 
					playTimer();
		        }
			}, currentMacro.getMillis());
		}
		else
		{
			if(UI.loopCheck.isSelected())
			{
				updateMacroList(UI.macroList);
				playTimer();
			}
			else
			{
				UI.enableTabs();
			}
		}
	}
	
	public void stopTimer()
	{
		if(timer != null)
		{
			timer.cancel();
			timer.purge();
		}
	}
	
	public void updateMacroList(ArrayList<Macro> q)
	{
		macros = new ArrayList<Macro>();
		for(Macro m : q)
		{
			macros.add(m);
		}
		currentMacro = macros.get(0);
	}
	
	//returns true if all macros have been executed
	public boolean finishedRun()
	{
		return macros.size() < 1;
	}
	
	public ArrayList<Macro> getMacros()
	{
		return macros;
	}
}
