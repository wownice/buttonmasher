import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{
	private UI ui;
	public static void main(String[] args)
	{
		Application.launch(args);	
	}
	
	public void start(Stage gloStage) throws Exception
	{
		ui = new UI(gloStage);
	}
	
	@Override
	public void stop()
	{
		ui.stopRun();
	}
}
