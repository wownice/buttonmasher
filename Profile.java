import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Profile 
{		
	public static ArrayList<Macro> readProfileFile(String fileName)
	{
		ArrayList<Macro> list = new ArrayList<Macro>();
		File profileFile = new File("profiles/" + fileName + ".prof");
		try 
		{
			BufferedReader readFile = new BufferedReader(new FileReader(profileFile));
			String[] semicolonSplit = readFile.readLine().split(";");
			Macro m;
			for(String s : semicolonSplit)
			{
				String[] individual = s.split(",");
				if(individual[0].equals("Mouse"))
				{
					m = new MouseMacro(UI.robot, "Mouse", Integer.parseInt(individual[3]), 
							Integer.parseInt(individual[1]), Integer.parseInt(individual[2]));
				}
				else
				{
					m = new KeyboardMacro(UI.robot, individual[0], Integer.parseInt(individual[1]));
				}
				list.add(m);
			}
			readFile.close();
		} catch (FileNotFoundException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
		return list;
	}
	
	public static void saveProfileFile(ArrayList<Macro> macros, String name)
	{
		File profileDir = new File("/profiles");
		profileDir.mkdir();
		
		File profileFile = new File("profiles/" + name + ".prof");
		try 
		{
			profileFile.createNewFile();
			String writeString;
			BufferedWriter writeFile;
			try 
			{
				writeFile = new BufferedWriter(new FileWriter(profileFile));
				for(Macro m : macros)
				{
					if(m.getKeycode().equals("Mouse"))
					{
						writeString = "Mouse, " + m.getX() + "," + m.getY() + "," + m.getMillis() + ";";
					}
					else
					{
						writeString = m.getKeycode() + "," + m.getMillis() + ";";
					}
					
					try 
					{
						writeFile.write(writeString);
					} catch (IOException e) {e.printStackTrace();}
				}
				try 
				{
					writeFile.close();
				} catch (IOException e1) {e1.printStackTrace();}
			} catch (IOException e2) {e2.printStackTrace();}		
		} catch (IOException e3) {e3.printStackTrace();}	
	}
	
	public static void deleteProfileFile(String name)
	{
		File profileFile = new File("profiles/" + name + ".prof");
		profileFile.delete();
	}
}
