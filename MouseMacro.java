import java.awt.Robot;
import java.awt.event.InputEvent;

public class MouseMacro extends Macro
{
	private int x;
	private int y;
	
	public MouseMacro(Robot rob, String key, int mil, int nX, int nY) 
	{
		super(rob, key, mil);
		x = nX;
		y = nY;
	}

	@Override
	public void pressButton() 
	{
		getRobot().mouseMove(x, y);
		getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);	
		getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);	
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
}
