import java.awt.Robot;

public class KeyboardMacro extends Macro
{

	public KeyboardMacro(Robot rob, String key, int mil) 
	{
		super(rob, key, mil);
	}

	@Override
	public void pressButton()
	{
		getRobot().keyPress(UI.ct.conversion.get(getKeycode()));	
		getRobot().keyRelease(UI.ct.conversion.get(getKeycode()));	
	}

	@Override
	public int getX(){return -1;}

	@Override
	public int getY() {return -1;}
}
