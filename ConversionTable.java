import java.awt.event.KeyEvent;
import java.util.LinkedHashMap;

public class ConversionTable 
{
	LinkedHashMap <String, Integer> conversion;
	Object[] keyConversion;
	public ConversionTable()
	{
		conversion = new LinkedHashMap<String, Integer>();
		conversion.put("1", KeyEvent.VK_1);
		conversion.put("2", KeyEvent.VK_2);
		conversion.put("3", KeyEvent.VK_3);
		conversion.put("4", KeyEvent.VK_4);
		conversion.put("5", KeyEvent.VK_5);
		conversion.put("6", KeyEvent.VK_6);
		conversion.put("7", KeyEvent.VK_7);
		conversion.put("8", KeyEvent.VK_8);
		conversion.put("9", KeyEvent.VK_9);
		conversion.put("0", KeyEvent.VK_0);
		conversion.put("-", KeyEvent.VK_SUBTRACT);
		conversion.put("=", KeyEvent.VK_EQUALS);
		conversion.put("A", KeyEvent.VK_A);
		conversion.put("B", KeyEvent.VK_B);
		conversion.put("C", KeyEvent.VK_C);
		conversion.put("D", KeyEvent.VK_D);
		conversion.put("E", KeyEvent.VK_E);
		conversion.put("F", KeyEvent.VK_F);
		conversion.put("G", KeyEvent.VK_G);
		conversion.put("H", KeyEvent.VK_H);
		conversion.put("I", KeyEvent.VK_I);
		conversion.put("J", KeyEvent.VK_J);
		conversion.put("K", KeyEvent.VK_K);
		conversion.put("L", KeyEvent.VK_L);
		conversion.put("M", KeyEvent.VK_M);
		conversion.put("N", KeyEvent.VK_N);
		conversion.put("O", KeyEvent.VK_O);
		conversion.put("P", KeyEvent.VK_P);
		conversion.put("Q", KeyEvent.VK_Q);
		conversion.put("R", KeyEvent.VK_R);
		conversion.put("S", KeyEvent.VK_S);
		conversion.put("T", KeyEvent.VK_T);
		conversion.put("U", KeyEvent.VK_U);
		conversion.put("V", KeyEvent.VK_V);
		conversion.put("W", KeyEvent.VK_W);
		conversion.put("X", KeyEvent.VK_X);
		conversion.put("Y", KeyEvent.VK_Y);
		conversion.put("Z", KeyEvent.VK_Z);
		conversion.put("[", KeyEvent.VK_OPEN_BRACKET);
		conversion.put("]", KeyEvent.VK_CLOSE_BRACKET);
		conversion.put("\\", KeyEvent.VK_BACK_SLASH);
		conversion.put(";", KeyEvent.VK_SEMICOLON);
		conversion.put(",", KeyEvent.VK_COMMA);
		conversion.put(".", KeyEvent.VK_PERIOD);
		conversion.put("/", KeyEvent.VK_SLASH);
		conversion.put("CONTROL", KeyEvent.VK_CONTROL);
		conversion.put("ALT", KeyEvent.VK_ALT);
		conversion.put("ENTER", KeyEvent.VK_ENTER);
		conversion.put("SHIFT", KeyEvent.VK_SHIFT);
		conversion.put("SPACE", KeyEvent.VK_SPACE);
		conversion.put("INSERT", KeyEvent.VK_INSERT);
		conversion.put("PAGE_UP", KeyEvent.VK_PAGE_UP);
		conversion.put("PAGE_DOWN", KeyEvent.VK_PAGE_DOWN);
		conversion.put("HOME", KeyEvent.VK_HOME);
		conversion.put("END", KeyEvent.VK_END);
		conversion.put("DELETE", KeyEvent.VK_DELETE);
		conversion.put("ESCAPE", KeyEvent.VK_ESCAPE);
		conversion.put("F1", KeyEvent.VK_F1);
		conversion.put("F2", KeyEvent.VK_F2);
		conversion.put("F3", KeyEvent.VK_F3);
		conversion.put("F4", KeyEvent.VK_F4);
		conversion.put("F5", KeyEvent.VK_F5);
		conversion.put("F6", KeyEvent.VK_F6);
		conversion.put("F7", KeyEvent.VK_F7);
		conversion.put("F8", KeyEvent.VK_F8);
		conversion.put("F9", KeyEvent.VK_F9);
		conversion.put("F10", KeyEvent.VK_F10);
		conversion.put("F11", KeyEvent.VK_F11);
		conversion.put("F12", KeyEvent.VK_F12);
		
		keyConversion = conversion.keySet().toArray();
	}
}
